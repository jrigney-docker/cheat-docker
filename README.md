# Cheat
cheat(https://github.com/chrisallenlane/cheat) in a container.

## build
```
docker build -t jrigney/cheat .
```

## run

```
docker run --rm -e EDITOR=vim --mount source=cheatdata,target=/root/.cheat -it jrigney/cheat:2.2.3-ff52d7c $*
```

if you want to enter the container for debugging

` docker run --rm -e EDITOR=vim -v cheatdata:/root/.cheat -it --entrypoint bash jrigney/cheat:2.2.3-ff52d7c`

## Cheat data volume
Create a volume to store the cheat data
`docker volume create cheatdata`

to see what is in the volume
`sudo ls /var/lib/docker/volumes/cheatdata/_data`

copy data into the data volume

When cp the data from the hostfile system to the container you must cp the files don't cp a link.

```
docker container create --name dummy -v cheatdata:/data alpine
docker cp ~/preferences/shared/cheat/dotcheat/. dummy:/data
docker rm dummy
```
or

`docker run --rm -v $PWD:/source -v my_volume:/dest -w /source alpine cp myfile.txt /dest`


## backup cheatdata
```
docker container create --name dummy -v cheatdata:/data alpine
docker run --rm --volumes-from dummy -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /data
docker rm dummy
```
